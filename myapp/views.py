from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Data
from django.core import serializers
from django.http import JsonResponse

# Create your views here.
@csrf_exempt
def index(request):

    top5 = {'context' : Data.objects.all()[:5]}
    if request.method == 'POST':
        title = request.POST["title"]
        author = request.POST["author[]"]
        likes = int(request.POST["likes"]) 
        if likes == 1:
            Data.objects.create(
                title = title,
                author = author,
                likes= likes 
                )
            print(Data.objects.all())
            

        else: 
            fetched_data = Data.objects.get(title = title)
            fetched_data.likes = likes
            print(fetched_data.likes)
            
            
    else:
        Data.objects.all().delete()

    return render(request, 'index.html', top5)
